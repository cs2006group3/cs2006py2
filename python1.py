import pandas as pd
import numpy as np


#Locate and read file.
LocationOfDataFile= r'/cs/home/sjm34/Documents/GitProjects/CS2006Python/Tweets.csv'
df=pd.read_csv(LocationOfDataFile)
df.drop_duplicates()


#Calc number of tweets/retweets/replies.
NumberOfTweets=len(df)
NumberOfReTweets=df.text.str.contains('RT @').sum()
NumberOfReplies=df.in_reply_to_screen_name.count()
print'Number of tweets :', NumberOfTweets
print'Number of retweets :', NumberOfReTweets
print'Number of replies :', NumberOfReplies


#Average number of tweets and standart deviation.
groups=df.groupby('from_user')
MeansOfTweets=groups.size().mean()
StandartDeviationOfTweets=groups.size().std()
print'Mean for the number of tweets per user',round(MeansOfTweets,4)
print'Standart deviation for the number of tweets per user',round(StandartDeviationOfTweets,4)
